module.exports = {
    "parserOptions": {
      "ecmaVersion": 2018,
      "sourceType": "module"
    },
    "env": {
      "browser": true,
      "node": true,
    },
    parser: "babel-eslint",
    "rules": {
      "array-bracket-spacing": ["error", "never"],
      // "array-callback-return": "error",
      "eol-last": "error",
      // "func-style": ["error", "expression"],
      "indent": ["error", 2, {
        "SwitchCase": 1,
        "VariableDeclarator": { "var": 2, "let": 2, "const": 3 },
        "outerIIFEBody": 1,
        "MemberExpression": 1,
        "FunctionDeclaration": { "parameters": 1, "body": 1 },
        "FunctionExpression": { "parameters": 1, "body": 1 },
        "CallExpression": { "arguments": 1 },
        "ArrayExpression": 1,
        "ObjectExpression": 1,
        "ImportDeclaration": 1,
        "flatTernaryExpressions": false,
        "ignoreComments": false
      }],
      "keyword-spacing": ["error", { "before": true, "after": true }],
      // "max-len": ["error", { "code": 100, "ignoreTemplateLiterals": true }],
      "newline-per-chained-call": "off",
      "no-array-constructor": "error",
      "no-console": 0,
      "no-eval": "error",
      // "no-loop-func": "error",
      "no-new-object": "error",
      // "no-param-reassign": "error",
      // "no-useless-escape": "error",
      "no-whitespace-before-property": "error",
      "object-curly-spacing": ["error", "never"],
      // "object-shorthand": "error", // es6
      "padded-blocks": ["error", { "blocks": "never", "switches": "never", "classes": "never" }],
      "padding-line-between-statements": [
        "error",
        { blankLine: "always", prev: "*", next: "return" },
        { blankLine: "always", prev: ["const", "let", "var"], next: "*"},
        { blankLine: "any",    prev: ["const", "let", "var"], next: ["const", "let", "var"]},
        { blankLine: "always", prev: "directive", next: "*" },
        { blankLine: "any",    prev: "directive", next: "directive" }
      ],
      // "prefer-destructuring": ["error", { "array": true, "object": true }, { "enforceForRenamedProperties": false }],
      // "prefer-rest-params": "error",
      // "prefer-spread": "error",
      // "prefer-template": "error",
      "quotes": ["error", "single", { "avoidEscape": true, "allowTemplateLiterals": true }],
      "quote-props": ["error", "as-needed"],
      "semi": [
        "error",
        "always"
      ],
      "space-before-blocks": ["error", "always"],
      "space-before-function-paren": ["error", {"anonymous": "always", "named": "never", "asyncArrow": "always"}],
      "space-in-parens": ["error", "never"],
      "space-infix-ops": "error",
      "spaced-comment": ["error", "always", {
        "block": { "exceptions": ["*"], "balanced": true }
      }],
      // "template-curly-spacing": "error",
      // "wrap-iife": ["error", "any", { "functionPrototypeMethods": true }],
    }
  };
  