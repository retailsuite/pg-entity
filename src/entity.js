'use strict';

const db = require('./db');

// tblCurrency = new Table('t_currency');
// ...get table description from server....
// tblCurrency.filter().orderBy().get();
// tblCurrency.get(id);
//    .filter('columnA').gte(500)
//    .filter('columnB').like('halo')
//    .sort('columnA')
//    .group('ColumnA','ColumnC')
//    .get()
// .insert(row);
// .insert(row2);
// .commit();


/**
 *  @class
 *  Поддерживаются обычные select с join/left join,
 *  insert / update одной записи.
 *  Использовать для примитивной логики.
 *  Сложные обработки необходимо реализовывать в хранимых процедурах new Procedure('proc_name');
 *  Основная проверка на injection реализована в серверной части (в хранимой процедуре orm)
 *  Выполнение select запроса происходит вызовом commit(), возвращающий promise
 */
module.exports = class Table {
  // endregion
  constructor(tableName) {
    this.rows = [];
    this.selectCol = [' * '];
    this.prepQuery = '';
    this.skipQuery = '';
    this.sortQuery = '';
    this.joinQuery = '';
    this.leftQuery = '';
    this.limitQuery = '';
    this.whereQuery = '';
    this.tableName = tableName;
    this.recordset = [];
  }
  /**
     *
     * @param col
     * @returns {Table}
     */
  select(...col) {
    // this.selectCol = col.map(c => { return ` ${this.tableName}.${c} `});
    this.selectCol = col.map(c => { return ` ${c} `; });

    return this;
  }
  /**
     * Очистить все накопленные фильтры (обнуляем whereQuery)
     * @returns {Table}
     */
  clear() {
    this.whereQuery = '';

    return this;
  }
  filter(column) {
    /*
        Подготовка where сегмента.
         */
    // TODO реализовать нужные операторы
    // TODO проверка колонок

    // if (!column) throw new Error('Please, provide proper column name');

    let checkVal = function (v) {
      if (v === undefined) 
        return ' ';

      if (v === null)
        return 'null';

      if (typeof v === 'object')
        throw new Error('value must be string or nubmer');

      if (typeof v !== 'number')
        return "''" + v.toString() + "''";

      if (Array.isArray(v))
        return "''" + v.join(',').toString() + "''";

      return v.toString();
    };

    return {

      raw: (q) => {
        this.whereQuery += ` and ${q} `;

        return this;
      },

      isnull: () => {
        this.whereQuery += ` and ${column} is null `;

        return this;
      },
      notnull: () => {
        this.whereQuery += ` and ${column} is not null `;

        return this;
      },
      eq: val => {
        if (!val) return console.trace('val is not defined!');
        val = checkVal(val);
        this.whereQuery += ` and ${column} = ${val} `;

        return this;
      },
      neq: val => {
        if (!val) return console.trace('val is not defined!');
        val = checkVal(val);
        this.whereQuery += ` and ${column} <> ${val} `;

        return this;
      },
      gte: val => {
        if (!val) return console.trace('val is not defined!');
        val = checkVal(val);
        // check val is number();
        this.whereQuery += ` and ${column} >= ${val} `;

        return this;
      },
      gt: val => {
        if (!val) return console.trace('val is not defined!');
        val = checkVal(val);
        // check val is number();
        this.whereQuery += ` and ${column} > ${val} `;

        return this;
      },
      lte: val => {
        if (!val) return console.trace('val is not defined!');
        val = checkVal(val);
        this.whereQuery += ` and ${column} <= ${val} `;

        return this;
      },
      lt: val => {
        if (!val) return console.trace('val is not defined!');
        val = checkVal(val);
        this.whereQuery += ` and ${column} < ${val} `;

        return this;
      },
      like: val => {
        if (!val) return console.trace('val is not defined!');
        val = checkVal(val);
        this.whereQuery += ` and ${column} like ${val} `;

        return this;
      },
      between: (val1, val2) => {
        if (!val1 || !val2) return console.trace('val is not defined!');
        val1 = checkVal(val1);
        val2 = checkVal(val2);
        this.whereQuery += ` and ${column} between (${val1}, ${val2}) `;

        return this;
      },
      in: (...vals) => {
        if (!vals) return console.trace('vals is not defined!');
        vals = vals.map(v => {
          return checkVal(v);
        });
        this.whereQuery += ` and ${column} in (${vals.join(', ')}) `;

        return this;
      },
      notin: (...vals) => {
        if (!vals) return console.trace('vals is not defined!');
        vals = vals.map(v => {
          return checkVal(v);
        });
        this.whereQuery += ` and ${column} not in (${vals.join(', ')}) `;

        return this;
      },
      match: val => {
        if (!val) return console.trace('val is not defined!');
        val = checkVal(val);

        // TODO: @Serg - вынести метод в отдельный файл escapeSql(val)
        let valEscape = val => val.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, '\\$1');

        // Экранируем служебные символы regexp pattern
        // const badChars = ['+', '?', '%', '$', '*', '^', '.', '{', '|'];
        // Список служебных символов regexp pattern для экранирования.
        // let valEscape = (val) => {
        //   for (let i = badChars.length - 1; i--;) {
        //     if (val.indexOf(badChars[i]) > -1) {
        //       return val.replace(badChars[i], `\\${badChars[i]}`);
        //     }
        //   }

        //   return val;
        // };

        this.whereQuery += ` and ${column} ~* ${valEscape(val)} `;

        return this;
      },
      notmatch: val => {
        if (!val) return console.trace('val is not defined!');
        val = checkVal(val);
        this.whereQuery += ` and ${column} !~* ${val} `;

        return this;
      },
    };
  }
  join(joinTable) {
    return {
      on: (column1, column2) => {
        if (!column1) throw new Error('Column for inner join is not defined.');
        if (!column2)
          column2 = column1;
        this.joinQuery += ` inner join ${joinTable} on ${column1} = ${column2}  `;

        return this;
      }
    };
  }
  left(leftTable) {
    return {
      on: (column1, column2) => {
        if (!column1) throw new Error('Column for left join is not defined.');
        if (!column2)
          column2 = column1;
        this.leftQuery += ` left join ${leftTable} on ${column1} = ${column2}  `;

        return this;
      },
      onRaw: (sql) => {
        if (!sql) throw new Error('Column for left join is not defined.');
        this.leftQuery += ` left join ${leftTable} on ${sql}  `;

        return this;
      }
    };
  }
  skip(val) {
    if (!(val || typeof val === 'number' || val === 0))
      throw new Error('value must be int');

    this.skipQuery = ` ${val}`;

    return this;
  }
  limit(val) {
    if (!(val || typeof val === 'number' || val === 0))
      throw new Error('value must be int');
    // this.skipQuery = ` ${val}`;
    this.limitQuery = ` ${val}`;

    return this;
  }
  sort(...val) {
    if (val.length < 1) 
      throw new Error('value must be int');
    this.sortQuery = ` ${val.join(', ')} `;

    return this;
  }
  commit() {
    let opts = {
      select: this.selectCol.join(', '),
      where:  this.whereQuery,
      join:   this.joinQuery,
      left:   this.leftQuery,
      sort:   this.sortQuery,
      skip:   this.skipQuery,
      limit:  this.limitQuery
    };

    // TODO вернуть проmис
    return db.orm(this.tableName, 'select', opts);
    // orm(this.tableName, 'select', opts)
    //     .then(res=>console.log(res))
    //     .catch(err=>console.log(err));
  }
  /**
     * Для тестирования
     */
  log() {
    this.commit().then(res => {
      console.log(res);
    });
  }
  /**
     *  Вставка значения в таблицу
     * @param f ['col', 'col2'] or {'col1':'val1', 'col2':'val2'}
     * @param v ['val1', 'val2']
     * @returns {Promise<T>}
     */
  insert(f, v) {
    // insert 1 строки, возвращает вставленную строку
    let opts = {}, fields = [], // [col1, col2, col3]
        values = []; // [val1, val2, val3]

    if (Array.isArray(f) && Array.isArray(v)) {
      [fields, values] = [f, v];
    }
    else if (typeof f === 'object' && !v) {
      Object.keys(f).map(key => {
        fields.push(key);
        values.push(f[key]);
      });
    }
    else {
      throw new Error('Provide proper fields and values to insert');
    }
    // TODO проверка столбцов и значений
    fields.map(f => {
      if (typeof f !== 'string')
        throw new Error('Columns names must be string');
    });
    values = values.map(v => {
      if (v === undefined || v === null)
        return 'null';
      if (typeof v === 'string' || typeof v === 'object') {
        v = `''${v.toString().replace(/\'/g,'"') }''`;
      }

      return v;
    });
    opts['fields'] = fields.join(', ');
    opts['values'] = values.join(', ');

    return db.orm(this.tableName, 'insert', opts);
  }
  /**
     *
     * @param pk    id обновляемой записи
     * @param f ['col', 'col2'] or {'col1':'val1', 'col2':'val2'}
     * @param v ['val1', 'val2']
     * @returns {Table}
     */
  update(pk, f, v) {
    // update только 1 строки по ID, возвращает обновленную строку
    if (!pk || typeof pk !== 'number')
      throw new Error('Cant update without proper row ID');
    let opts = {}, fields = [], // [col1, col2, col3]
        values = []; // [val1, val2, val3]

    if (Array.isArray(f) && Array.isArray(v)) {
      [fields, values] = [f, v];
    }
    else if (typeof f === 'object' && !v) {
      Object.keys(f).map(key => {
        if (key === 'row_id' || key === '__row_count') // TODO - сообщение об ошибке!
          return;
        fields.push(key);
        values.push(f[key]);
      });
    }
    else {
      throw new Error('Provide proper fields and values to insert');
    }
    // TODO проверка столбцов и значений
    fields.map(f => {
      if (typeof f !== 'string')
        throw new Error('Columns names must be string');
    });
    values = values.map(v => {
      if (v === undefined || v === null)
        return 'null';
      if (typeof v === 'string' || typeof v === 'object') {
        v = `''${v.toString().replace(/\'/g, '"')}''`;
      }

      return v;
    });
    opts['oid'] = pk;
    opts['fields'] = fields.join(', ');
    opts['values'] = values.join(', ');

    return db.orm(this.tableName, 'update', opts);
  }

  remove(pk) {
    if (!pk)
      throw new Error('Cant remove without ID');
    if (typeof pk !== 'number')
      throw new Error('Row id must be integer');
    let opts = {
      oid: pk
    };

    return db.orm(this.tableName, 'delete', opts);
  }
  delete(id) {
    return this.update(id, ['deleted'], [true]).commit();
  }
};
